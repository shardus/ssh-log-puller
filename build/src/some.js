"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable node/no-unsupported-features/node-builtins */
const blessed = require("blessed");
const fs_1 = require("fs");
const node_ssh_1 = require("node-ssh");
const pLimit = require("p-limit");
// Parse CLI args
const key = process.argv[2];
const nodeIps = process.argv.slice(3);
// Main program
async function main() {
    // Get report from monitor
    /*
    console.log('Getting report from monitor...');
    const report: MonitorReport = await got(monitorReportUrl).json();
  
    // Parse report into nodelist of IPs
    const nodelist = Object.values(report.nodes).reduce(
      (accumulator: string[], status) =>
        accumulator.concat(
          Object.values(status).map(info => `${info.nodeIpInfo.externalIp}`)
        ),
      []
    );
    */
    const nodelist = nodeIps;
    // Start blessed screen
    const { screen, logger } = startBlessed();
    // Concurrently run ssh connections
    const limit = pLimit(10);
    const tasks = [];
    const results = [];
    for (let idx = 0; idx < nodelist.length; idx++) {
        const node = nodelist[idx];
        const update = updateFactory(logger, idx);
        tasks.push(limit(async () => {
            // SSH into node
            update(`${node} started...`);
            const ssh = new node_ssh_1.NodeSSH();
            await ssh.connect({
                host: node,
                username: 'ec2-user',
                privateKey: key,
            });
            // // Delete files in home dir
            // update(`${node} deleting files...`);
            // await ssh.execCommand('rm -rf ./logs ./db ./statistics.tsv');
            // Copy files from docker container onto remote filesystem
            update(`${node} copying from docker container...`);
            await ssh.execCommand('docker cp $(docker ps -a -q  --filter ancestor=registry.gitlab.com/liberdus/server:dev):/usr/src/app/logs .');
            await ssh.execCommand('docker cp $(docker ps -a -q  --filter ancestor=registry.gitlab.com/liberdus/server:dev):/usr/src/app/db .');
            // Copy files from remote to local
            update(`${node} downloading from server...`);
            await fs_1.promises.mkdir(`./instances/${node}/logs`, { recursive: true });
            await ssh.getDirectory(`./instances/${node}/logs`, '/home/ec2-user/logs', {
                recursive: true,
            });
            await fs_1.promises.mkdir(`./instances/${node}/db`, { recursive: true });
            await ssh.getDirectory(`./instances/${node}/logs`, '/home/ec2-user/db', {
                recursive: true,
            });
            // Close the connection
            ssh.dispose();
            update(`${node} done`);
        }));
    }
    await Promise.all(tasks);
    // End blessed screen
    endBlessed(screen, logger);
    // Print results
    console.log();
    console.log(results);
}
main();
function updateFactory(logger, line) {
    return (msg) => {
        logger.setLine(line, `(${line + 1}) ${msg}`);
    };
}
function startBlessed() {
    const screen = blessed.screen({
        smartCSR: true,
    });
    const logger = blessed.log({
        tags: true,
        keys: true,
        vi: true,
        mouse: true,
        scrollback: Infinity,
        scrollbar: {
            ch: ' ',
            track: {
                bg: 'yellow',
            },
            style: {
                inverse: true,
            },
        },
    });
    screen.append(logger);
    screen.key('q', () => endBlessed(screen, logger));
    logger.focus();
    screen.render();
    return { screen, logger };
}
function endBlessed(screen, logger) {
    const text = logger.getText();
    screen.destroy();
    console.log(text);
}
//# sourceMappingURL=some.js.map