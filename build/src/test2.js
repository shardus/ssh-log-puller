"use strict";
process.stdin.resume();
process.stdin.setEncoding('utf8');
console.log('This is interactive console for cursorTo explanation');
process.stdin.on('data', data => {
    // This is when only x value is given as input
    for (let i = 0; i < 30; i++) {
        console.log('here x = ' + i + ' and y = 0');
        require('readline').cursorTo(process.stdout, i);
    }
    // This is when  x  and y values are given as input
    // for(i = 0; i< 10; i++){
    //     console.log('here x = '+ i + ' and y = '+ i );
    //     require('readline').cursorTo(process.stdout, i, i);
    // }
});
process.on('SIGINT', () => {
    process.stdout.write('\n end \n');
    // eslint-disable-next-line no-process-exit
    process.exit();
});
//# sourceMappingURL=test2.js.map