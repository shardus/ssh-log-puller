"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const charmFactory = require("charm");
const charm = charmFactory();
charm.pipe(process.stdout);
charm.reset();
function updateFactory(line) {
    return (msg) => {
        charm.position(0, line);
        charm.write(msg);
        charm.position(0, 0);
    };
}
for (let i = 1; i <= 10; i++) {
    const update = updateFactory(i);
    let counter = 0;
    setInterval(() => {
        counter++;
        update(`counter: ${counter}`);
    }, 1000 + 100 * i);
}
/*
const colors: charmFactory.CharmColor[] = [
  'red',
  'cyan',
  'yellow',
  'green',
  'blue',
];
const text = 'Always after me lucky charms.';

let offset = 0;
setInterval(() => {
  let y = 0,
    dy = 1;
  for (let i = 0; i < 40; i++) {
    const color = colors[(i + offset) % colors.length];
    const c = text[(i + offset) % text.length];
    charm.move(1, dy).foreground(color).write(c);
    y += dy;
    if (y <= 0 || y >= 5) dy *= -1;
  }
  charm.position(0, 1);
  offset++;
}, 150);
*/
//# sourceMappingURL=test.js.map