/* eslint-disable node/no-unsupported-features/node-builtins */
import * as blessed from 'blessed';
import {promises as fsp} from 'fs';
import got from 'got/dist/source';
import {NodeSSH, SSHExecCommandResponse} from 'node-ssh';
import * as pLimit from 'p-limit';
import {clearLine} from 'readline';

// Define interfaces
interface NodeIpInfo {
  externalIp: string;
  externalPort: number;
  internalIp: string;
  internalPort: number;
}

interface MonitorReport {
  nodes: {
    joining: {[publicKey: string]: {nodeIpInfo: NodeIpInfo}};
    syncing: {[publicKey: string]: {nodeIpInfo: NodeIpInfo}};
    active: {[publicKey: string]: {nodeIpInfo: NodeIpInfo}};
  };
  totalInjected: unknown;
  totalRejected: unknown;
  totalExpired: unknown;
  totalProcessed: unknown;
  timestamp: unknown;
}

// Parse CLI args
const key = process.argv[2];
const monitorReportUrl = process.argv[3];

// Main program
async function main() {
  // Get report from monitor
  console.log('Getting report from monitor...');
  const report: MonitorReport = await got(monitorReportUrl).json();

  // Parse report into nodelist of IPs
  const nodelist = Object.values(report.nodes).reduce(
    (accumulator: string[], status) =>
      accumulator.concat(
        Object.values(status).map(info => `${info.nodeIpInfo.externalIp}`)
      ),
    []
  );

  // Start blessed screen
  const {screen, logger} = startBlessed();

  // Concurrently run ssh connections
  const limit = pLimit(10);
  const tasks = [];
  const results: SSHExecCommandResponse[] = [];

  for (let idx = 0; idx < nodelist.length; idx++) {
    const node = nodelist[idx];
    const update = updateFactory(logger, idx);

    tasks.push(
      limit(async () => {
        // SSH into node
        update(`${node} started...`);
        const ssh = new NodeSSH();
        await ssh.connect({
          host: node,
          username: 'ec2-user',
          privateKey: key,
        });

        // // Delete files in home dir
        // update(`${node} deleting files...`);
        // await ssh.execCommand('rm -rf ./logs ./db ./statistics.tsv');

        // Copy files from docker container onto remote filesystem
        update(`${node} copying from docker container...`);
        try {
          await ssh.execCommand(
            'docker cp $(docker ps -a -q  --filter ancestor=registry.gitlab.com/liberdus/server:dev):/usr/src/app/logs .'
          );
        } catch (err) {}

        try {
          await ssh.execCommand(
            'docker cp $(docker ps -a -q  --filter ancestor=registry.gitlab.com/liberdus/server:dev):/usr/src/app/db .'
          );
        } catch (error) {}

        // Copy files from remote to local
        update(`${node} downloading from server...`);
        await fsp.mkdir(`./instances/${node}/logs`, {recursive: true});
        try {
          await ssh.getDirectory(
            `./instances/${node}/logs`,
            '/home/ec2-user/logs',
            {
              recursive: true,
            }
          );
        } catch (err) {}
        await fsp.mkdir(`./instances/${node}/db`, {recursive: true});
        try {
          await ssh.getDirectory(
            `./instances/${node}/logs`,
            '/home/ec2-user/db',
            {
              recursive: true,
            }
          );
        } catch (err) {}

        // Close the connection
        ssh.dispose();
        update(`${node} done`);
      })
    );
  }

  await Promise.all(tasks);

  // End blessed screen
  endBlessed(screen, logger);

  // Print results
  console.log();
  console.log(results);
}

main();

function updateFactory(logger: blessed.Widgets.Log, line: number) {
  return (msg: string | string[]) => {
    logger.setLine(line, `(${line + 1}) ${msg}`);
  };
}

function startBlessed() {
  const screen = blessed.screen({
    smartCSR: true,
  });

  const logger = blessed.log({
    tags: true,
    keys: true,
    vi: true,
    mouse: true,
    scrollback: Infinity,
    scrollbar: {
      ch: ' ',
      track: {
        bg: 'yellow',
      },
      style: {
        inverse: true,
      },
    },
  });

  screen.append(logger);
  screen.key('q', () => endBlessed(screen, logger));

  logger.focus();
  screen.render();

  return {screen, logger};
}

function endBlessed(
  screen: blessed.Widgets.Screen,
  logger: blessed.Widgets.Log
) {
  const text = logger.getText();
  screen.destroy();
  console.log(text);
}
