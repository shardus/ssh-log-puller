import * as blessed from 'blessed';
const sleep = require('util').promisify(setTimeout);

const screen = blessed.screen({
  smartCSR: true,
});

screen.key('q', exit);

const logger = blessed.log({
  tags: true,
  keys: true,
  vi: true,
  mouse: true,
  scrollback: Infinity,
  scrollbar: {
    ch: ' ',
    track: {
      bg: 'yellow',
    },
    style: {
      inverse: true,
    },
  },
});

screen.append(logger);

logger.focus();

screen.render();

async function main() {
  for (let i = 0; i < 40; i++) {
    const update = updateFactory(i);
    update(`Started ${i}...`);
    setTimeout(() => update(`${i} done`), 3000).unref();
    await sleep(1000);
  }

  exit();
}

main();

function updateFactory(line: number) {
  return (msg: string | string[]) => {
    logger.setLine(line, msg);
  };
}

function exit() {
  const text = logger.getText();
  screen.destroy();
  console.log(text);
}
